import SwiftUI

public struct RoundedRectangleView: View {
    
    private let configuration: Configuration
    
    public init(title: String) {
        self.init(configuration: Configuration(title: title))
    }
    
    private init(configuration: Configuration) {
        self.configuration = configuration
    }
    
    public var body: some View {
        HStack(spacing: 20) {
            configuration.image?()
            VStack(alignment: .leading) {
                Text(configuration.title)
                    .font(.system(size: 22, weight: .medium))
                    .multilineTextAlignment(.leading)
                if let subtitle = configuration.subtitle {
                    Text(subtitle)
                        .font(.body)
                        .multilineTextAlignment(.leading)
                }
            }
            Spacer()
        }
        .padding()
        .background(configuration.backgroundColor ?? Color(hex: "edede9"))
        .clipShape(RoundedRectangle(cornerRadius: 15))
    }
}

// MARK: - Configuration

extension RoundedRectangleView {
    struct Configuration {
        let title: String
        var subtitle: String?
        var image: (() -> AnyView)?
        var backgroundColor: Color?
    }
}

// MARK: - Modifiers

extension RoundedRectangleView {
    
    /// Adds subtitle to this view.
    func subtitle(_ text: String?) -> RoundedRectangleView {
        var config = configuration
        if let subtitle = text {
            config.subtitle = subtitle
        }
        return RoundedRectangleView(configuration: config)
    }
    
    func backgroundColor(_ color: Color?) -> RoundedRectangleView {
        var config = configuration
        if let bgColor = color {
            config.backgroundColor = bgColor
        }
        return RoundedRectangleView(configuration: config)
    }
    
    /// Adds image to this view.
    func image<Content: View>(@ViewBuilder _ content: @escaping () -> Content?) -> RoundedRectangleView {
        var config = configuration
        config.image = {
            AnyView(content())
        }
        return RoundedRectangleView(configuration: config)
    }
}


struct RoundedRectangleView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            RoundedRectangleView(title: "Title")
            RoundedRectangleView(title: "Title")
                .subtitle("Subtitle")
            RoundedRectangleView(title: "Title")
                .subtitle("Subtitle")
                .image {
                    Image(systemName: "sun.max")
                        .font(.system(size: 30))
                        .frame(width: 30, height: 30)
                        .padding()
                        .background(Color.blue)
                        .clipShape(Circle())
                }
            RoundedRectangleView(title: "Title")
                .subtitle("Subtitle")
                .backgroundColor(Color(hex: "bde0fe"))
                .image {
                    Image(systemName: "sun.max")
                        .font(.system(size: 30))
                        .frame(width: 30, height: 30)
                        .padding()
                        .background(Color.blue)
                        .clipShape(Circle())
                }
        }.padding()
    }
}
