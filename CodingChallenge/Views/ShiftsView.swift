import Combine
import SwiftUI

struct ShiftsView<ViewModel>: View where ViewModel: ShiftsViewModelProtocol {

    @ObservedObject var viewModel: ViewModel
    @State private var showError = false
    @State private var errorMessage = ""

    var body: some View {
        NavigationView {
            Group {
                ZStack {
                    List {
                        ForEach(viewModel.sections) { section in
                            Section {
                                ForEach(section.shifts) { shift in
                                    shiftRowItemView(shift)
                                }
                            } header: {
                                Text(section.dateLabel)
                                    .font(.headline)
                            }
                        }
                    }.listStyle(SidebarListStyle())
                    Text(errorMessage)
                        .opacity(showError ? 1 : 0)
                }
            }
            .animation(.none, value: !viewModel.isLoading)
            .opacity(viewModel.isLoading ? 0 : 1)
            .animation(.default, value: viewModel.isLoading)
            .overlay(progressView)
            .onReceive(viewModel.onError) { message in
                errorMessage = message
                showError = true
            }
            .navigationTitle("Shifts")
        }
    }
    
    var progressView: some View {
        Group {
            if viewModel.isLoading {
                ProgressView().progressViewStyle(CircularProgressViewStyle())
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .ignoresSafeArea()
    }
}

// MARK: - Subviews
extension ShiftsView {
    func shiftRowItemView(_ shift: Shift) -> some View {
        Button(action: {
            viewModel.didSelectShift(shift)
        }) {
            HStack {
                VStack(alignment:.leading, spacing: 2) {
                    Text(String(format: "Distance %.0f km", shift.distance))
                        .foregroundColor(shift.distanceColor)
                    Text(shift.startTimeLabel + " - " + shift.endTimeLabel)
                        .foregroundColor(.black)
                        .font(.caption)
                }
                Spacer()
                Image(systemName: shift.iconName)
            }
        }
    }
}

struct ShiftsView_Previews: PreviewProvider {
    static var previews: some View {
        ShiftsView(viewModel: MockViewModel())
    }

    private class MockViewModel: ShiftsViewModelProtocol {
        var activeLink: ContentLink?

        var sections: [DayShifts] {
            [
                DayShifts(date: "8 January", shifts: [
                    Shift(id: 2,
                          shiftType: .day,
                          distance: 10,
                          startTime: Date(),
                          endTime: Date(),
                          details: nil)
                ]),
                DayShifts(date: "9 January", shifts: [
                    Shift(id: 3,
                          shiftType: .day,
                          distance: 20,
                          startTime: Date(),
                          endTime: Date(),
                         details: nil),
                    Shift(id: 4,
                          shiftType: .day,
                          distance: 600,
                          startTime: Date(),
                          endTime: Date(),
                         details: nil)
                ])
            ]
        }

        lazy var onError = PassthroughSubject<String, Never>().eraseToAnyPublisher()
        var isLoading = false
        func didSelectShift(_ shift: Shift) {}
    }
}
