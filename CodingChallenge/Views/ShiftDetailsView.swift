import SwiftUI

struct ShiftDetailsView<ViewModel>: View where ViewModel: ShiftDetailsViewModelProtocol  {
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        VStack {
            ZStack {
                Color(hex: viewModel.color)
                    .frame(height: 300)
                Image(systemName: viewModel.iconName)
                    .font(.system(size: 200))
                    .foregroundColor(.white)
            }
            VStack(alignment: .leading) {
                headerView()
                detailsList()
            }
            .padding()
            Spacer()
        }
        
    }
}

// MARK: - Subviews

extension ShiftDetailsView {
    func headerView() -> some View {
        VStack(alignment: .leading) {
            Text(viewModel.header)
                .font(.title)
            HStack {
                Text(viewModel.timeLabel)
                    .foregroundColor(.black)
                    .font(.caption)
                Spacer()
                Text(viewModel.rightLabel)
            }
            .font(.subheadline)
            .foregroundColor(.secondary)
            
            Divider()
        }
    }
    
    func detailsList() -> some View {
        VStack {
            ForEach(viewModel.detailsItems) { item in
                RoundedRectangleView(title: item.name)
                    .subtitle(item.subtitle)
                    .backgroundColor(item.backgroundColor)
                    .image(item.image ?? {
                        AnyView(EmptyView())
                    })
            }
        }
    }
}

struct ShiftDetailsView_Previews: PreviewProvider {
    static let mockedShift = Shift(
        id: 0,
        shiftType: .evening,
        distance: 20,
        startTime: Date(),
        endTime: Date(),
        details: ShiftDetails(
            covid: true, facility: nil, premium: true
        )
    )
    
    static var previews: some View {
        ShiftDetailsView(viewModel: ShiftDetailsViewModel(shift: mockedShift))
    }
}
