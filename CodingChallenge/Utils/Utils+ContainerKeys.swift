import Foundation

enum ContainerKeys: String, CodingKey {
    case data
}
