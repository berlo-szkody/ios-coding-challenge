import Foundation
import SwiftUI

extension Color {
    init(hex: String) {
        var newHex = hex
        if newHex.first == "#" {
            newHex = String(newHex.dropFirst())
        }
        let scanner = Scanner(string: newHex)
        var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff
        )
    }
}
