import SwiftUI

protocol ShiftDetailsViewModelProtocol: ObservableObject {
    var shift: Shift { get }
    var header: String { get }
    var timeLabel: String { get }
    var rightLabel: String { get }
    var iconName: String { get }
    var color: String { get }
    var detailsItems: [ShiftDetailsViewModel.DetailRowViewModel] { get }
}

final class ShiftDetailsViewModel: ShiftDetailsViewModelProtocol {
    
    struct DetailRowViewModel: Identifiable {
        let id = UUID()
        let name: String
        let subtitle: String?
        var image: (() -> AnyView)?
        let backgroundColor: Color?
    }
    
    let shift: Shift
    
    init(shift: Shift) {
        self.shift = shift
    }
    
    var header: String {
        shift.shiftType.rawValue
    }
    
    var timeLabel: String {
        shift.startTimeLabel + " - " + shift.endTimeLabel
    }
    
    var rightLabel: String {
        String(format: "Distance %.1f km", shift.distance)
    }
    
    var iconName: String {
        shift.iconName
    }
    
    var color: String {
        switch shift.shiftType {
        case .day:
            return "fcbf49"
        case .evening:
            return "9b2226"
        case .night:
            return "14213d"
        case .other:
            return "a3b18a"
        }
    }
    
    var premiumRow: DetailRowViewModel {
        let isPremium = shift.details?.premium ?? false
        let subtitle = isPremium ? "yes" : "no"
        let backgroundColor = isPremium ? Color.yellow : Color(hex: "e3d5ca")
        
        return .init(name: "Premium rate",
                     subtitle: subtitle,
                     image: {
            AnyView(Image(systemName: "dollarsign")
                .font(.system(size: 30))
                .frame(width: 30, height: 30)
                .padding()
                .background(Color.white)
                .clipShape(Circle()))
        },
                     backgroundColor: backgroundColor)
    }
    
    var facilityRow: DetailRowViewModel {
        let subtitle = shift.details?.facility?.name
        var color: Color?
        if let backgroundColor = shift.details?.facility?.color {
            color = Color(hex: backgroundColor)
        }
        return .init(name: "Facility type",
                     subtitle: subtitle,
                     image: nil,
                     backgroundColor: color)
    }
    
    var covidRow: DetailRowViewModel {
        let subtitle = shift.details?.covid == true ? "yes" : "no"
        let backgroundColor = Color.gray
        
        return .init(name: "Covid",
                     subtitle: subtitle,
                     backgroundColor: backgroundColor)
    }
    
    var detailsItems: [DetailRowViewModel] {
        [
            premiumRow,
            facilityRow,
            covidRow
        ]
    }
    
}
