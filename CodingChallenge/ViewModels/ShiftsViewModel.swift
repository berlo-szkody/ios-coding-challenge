import Combine
import Foundation

protocol ShiftsViewModelProtocol: ObservableObject, ContentFlowStateProtocol  {
    var onError: AnyPublisher<String, Never> { get }
    var sections: [DayShifts] { get }
    var isLoading: Bool { get }
    var activeLink: ContentLink? { get set }
    func didSelectShift(_ shift: Shift)
}

final class ShiftsViewModel: ShiftsViewModelProtocol {

    // MARK: - Properties
    
    lazy var onError = onErrorSubject.eraseToAnyPublisher()
    private let onErrorSubject = PassthroughSubject<String, Never>()
    
    @Published var sections: [DayShifts] = []
    @Published var isLoading: Bool = false
    @Published var activeLink: ContentLink?
    
    private var cancellables = Set<AnyCancellable>()
    private var shiftService: ShiftsServiceProtocol
    


    // MARK: - Initializer

    init(shiftService: ShiftsServiceProtocol) {
        self.shiftService = shiftService
        fetchShifts()
    }

    // MARK: - Input

    func didSelectShift(_ shift: Shift) {
        activeLink = .sheetLink(shift: shift)
    }
    
    // MARK: -
    
    private func fetchShifts() {
        isLoading = true
        
        shiftService.getShifts()
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                self?.isLoading  = false
                if case .failure(let error) = completion {
                    self?.onErrorSubject.send(error.localizedDescription)
                }
            } receiveValue: { [weak self] dayShifts in
                let sortedShifts = dayShifts.map { dayShift -> DayShifts in
                    let sortedShifts = dayShift.shifts.sorted { $0.distance < $1.distance }
                    return DayShifts(date: dayShift.date, shifts: sortedShifts)
                }
                self?.sections = sortedShifts
            }.store(in: &cancellables)
    }
    
    
}
