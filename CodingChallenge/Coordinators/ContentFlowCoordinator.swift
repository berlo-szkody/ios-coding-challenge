import SwiftUI

struct ContentFlowCoordinator<State: ContentFlowStateProtocol, Content: View>: View {

    @ObservedObject var state: State
    let content: () -> Content

    private var sheetItem: Binding<ContentLink?> {
        $state.activeLink
    }

    var body: some View {
        content()
            .sheet(item: sheetItem, content: sheetContent)
    }

    @ViewBuilder private func sheetContent(sheetItem: ContentLink) -> some View {
        switch sheetItem {
        case let .sheetLink(shift):
            ShiftDetailsView(viewModel: ShiftDetailsViewModel(shift: shift))
        }
    }
}
