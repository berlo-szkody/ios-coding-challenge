import Combine

protocol ContentFlowStateProtocol: ObservableObject {
    var activeLink: ContentLink? { get set }
}

enum ContentLink: Hashable, Identifiable {
  
    //could add cases with navigation links to another screens
    case sheetLink(shift: Shift)

    var sheetItem: ContentLink? {
        switch self {
            //in case of navigation link here would be nil
        case .sheetLink:
            return self
        }
    }

    var id: String {
        switch self {
        case let .sheetLink(shift):
            return "\(shift.id)"
        }
    }
    
    static func == (lhs: ContentLink, rhs: ContentLink) -> Bool {
        lhs.id == rhs.id
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}



