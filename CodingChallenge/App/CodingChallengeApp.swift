//
//  CodingChallengeApp.swift
//  CodingChallenge
//
//  Created by Brady Miller on 4/7/21.
//

import SwiftUI

@main
struct CodingChallengeApp: App {
    let service: ShiftsServiceProtocol
    let viewModel: ShiftsViewModel
    
    // MARK: - Init
    
    init() {
        service = ShiftsService()
        viewModel = ShiftsViewModel(shiftService: service)
    }

    var body: some Scene {
        WindowGroup {
            ContentFlowCoordinator(state: viewModel, content: content)
        }
    }
    
    @ViewBuilder private func content() -> some View {
        ShiftsView(viewModel: viewModel)
    }
}
