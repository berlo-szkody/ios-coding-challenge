import Foundation
import Combine

enum ServiceError: Error {
    case invalidURL
}
protocol ShiftsServiceProtocol {
    func getShifts() -> AnyPublisher<[DayShifts], Error>
}

public final class ShiftsService: ShiftsServiceProtocol {
    func getShifts() -> AnyPublisher<[DayShifts], Error> {
        // Could create an endpoint builder here depending on version, endpoint etc.
        guard var url = URL(string: Config.baseURLString + "v2/available_shifts") else { fatalError("Invalid url") }
        
        // Could make it as param and cleanup endpoints structure
        url = url.appending("address", value: "Dallas, TX")
        url = url.appending("type", value: "4day")
        return URLSession.shared
            .dataTaskPublisher(for: url)
            .map { $0.data }
            .decode(type: ResponseData.self, decoder: JSONDecoder())
            .map { $0.data }
            .map { $0.map { $0.toDomain() }}
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
    
    
}

struct ShiftsServiceMock: ShiftsServiceProtocol {
    var alwaysFails = false

    enum MockError: Error {
        case unknown
    }

    func getShifts() -> AnyPublisher<[DayShifts], Error> {
        guard !alwaysFails else {
            return Fail<[DayShifts], Error>(error: MockError.unknown).eraseToAnyPublisher()
        }
        var dayShifts = [DayShifts]()
        
        dayShifts.append(DayShifts(date: "8 January Sunday", shifts: [
            Shift(id: 2,
                  shiftType: .day,
                  distance: 10,
                  startTime: Date(),
                  endTime: Date(),
                  details: nil)
        ]))
        dayShifts.append(DayShifts(date: "9 January Sunday", shifts: [
            Shift(id: 3,
                  shiftType: .day,
                  distance: 20,
                  startTime: Date(),
                  endTime: Date(),
                  details: nil),
            Shift(id: 4,
                  shiftType: .night,
                  distance: 60,
                  startTime: Date(),
                  endTime: Date(),
                  details: nil),
            Shift(id: 5,
                  shiftType: .day,
                  distance: 600,
                  startTime: Date(),
                  endTime: Date(),
                  details: nil)
        ]))
        
        return Just(dayShifts).setFailureType(to: Error.self).eraseToAnyPublisher()
    }
}
