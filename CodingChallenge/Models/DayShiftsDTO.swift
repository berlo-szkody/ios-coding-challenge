import Foundation

struct ResponseData: Decodable {
    let data: [DayShiftsDTO]
}

struct DayShiftsDTO: Decodable {
    let date: String
    let shifts: [ShiftDTO]
}

extension DayShiftsDTO {
    func toDomain() -> DayShifts {
        return .init(date: date, shifts: shifts.map { $0.toDomain() })
    }
}
