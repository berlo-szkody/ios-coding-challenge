import Foundation

struct ShiftDTO {
    let id: Int
    let shiftType: String
    let startTime: String
    let endTime: String
    let distance: Double
    let covid: Bool
    let premium: Bool
    let facility: ShiftFacilityDTO
    
    struct ShiftFacilityDTO: Decodable {
        let name: String
        let color: String
    }
}

// MARK: - Decodable

extension ShiftDTO: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case id = "shift_id"
        case shiftType = "shift_kind"
        case startTime = "start_time"
        case endTime = "end_time"
        case distance = "within_distance"
        case covid = "covid"
        case premium = "premium_rate"
        case facility = "facility_type"
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        shiftType = try values.decode(String.self, forKey: .shiftType)
        startTime = try values.decode(String.self, forKey: .startTime)
        endTime = try values.decode(String.self, forKey: .endTime)
        distance = try values.decodeIfPresent(Double.self, forKey: .distance) ?? 0
        covid = try values.decodeIfPresent(Bool.self, forKey: .covid) ?? false
        premium = try values.decodeIfPresent(Bool.self, forKey: .premium) ?? false
        facility = try values.decodeIfPresent(ShiftFacilityDTO.self, forKey: .facility) ?? ShiftFacilityDTO(name: "", color: "")
    }
}

// MARK: - Domain mapper
extension ShiftDTO {
    func toDomain() -> Shift {
        var shiftTypeDomain: Shift.ShiftType
        switch shiftType {
        case "Day Shift":
            shiftTypeDomain = .day
        case "Night Shift":
            shiftTypeDomain = .night
        case "Evening Shift":
            shiftTypeDomain = .evening
        default:
            shiftTypeDomain = .other
        }
        

        let formatter = ISO8601DateFormatter()
        let startTimeDate = formatter.date(from: startTime) ?? Date()
        let endTimeDate = formatter.date(from: endTime) ?? Date()
        return .init(id: id,
                     shiftType: shiftTypeDomain,
                     distance: distance,
                     startTime: startTimeDate,
                     endTime: endTimeDate,
                     details: ShiftDetails(covid: covid, facility: .init(name: facility.name, color: facility.color), premium: premium)
        )
    }
}
