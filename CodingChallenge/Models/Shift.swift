import SwiftUI

struct Shift: Identifiable {
    
    enum ShiftType: String {
        case day = "Day shift"
        case night = "Night shift"
        case evening = "Evening shift"
        case other = "Shift"
    }
    
    let id: Int
    let shiftType: ShiftType
    let distance: Double
    let startTime: Date
    let endTime: Date
    let details: ShiftDetails?
  
    var iconName: String {
        switch shiftType {
        case .day:
            return "sun.max"
        case .night:
            return "moon"
        case .evening:
            return "sun.and.horizon"
        case .other:
            return "s.circle"
        }
    }
    
    var distanceColor: Color {
        switch distance {
        case 0...10:
            return Color.green
        case 11...50:
            return Color.yellow
        case 51...200:
            return Color.red
        default:
            return Color.black
        }
    }
    
    var startTimeLabel: String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: startTime)
    }
    
    var endTimeLabel: String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: endTime)
    }
    
}
