import Foundation

struct DayShifts: Identifiable {
    let id = UUID()
    let date: String 
    let shifts: [Shift]
    
    var dateLabel: String {
        let relativeDateFormatter = DateFormatter()
        relativeDateFormatter.timeStyle = .none
        relativeDateFormatter.dateStyle = .medium
        relativeDateFormatter.locale = Locale(identifier: "en_GB")
        relativeDateFormatter.doesRelativeDateFormatting = true
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        return relativeDateFormatter.string(from: inputFormatter.date(from: date) ?? Date())
    }
}
