import Foundation

struct ShiftDetails {
    let covid: Bool?
    let facility: ShiftFacility?
    let premium: Bool?
    
    struct ShiftFacility {
        let name: String
        let color: String
    }
}
