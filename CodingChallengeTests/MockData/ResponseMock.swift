import Foundation

enum MockData {
    static var response: Data {
    """
    {
        "data": [
            {
                "date": "2023-01-08",
                "shifts": [
                    {
                        "shift_id": 4755666,
                        "start_time": "2023-01-08T12:45:00+00:00",
                        "end_time": "2023-01-09T01:15:00+00:00",
                        "normalized_start_date_time": "2023-01-08 06:45:00",
                        "normalized_end_date_time": "2023-01-08 19:15:00",
                        "timezone": "Central",
                        "premium_rate": false,
                        "covid": false,
                        "shift_kind": "Day Shift",
                        "within_distance": 20,
                        "facility_type": {
                            "id": 2,
                            "name": "Hospital",
                            "color": "#FF9500"
                        },
                        "skill": {
                            "id": 9,
                            "name": "ER",
                            "color": "#007AFF"
                        },
                        "localized_specialty": {
                            "id": 209,
                            "specialty_id": 9,
                            "state_id": 44,
                            "name": "Registered Nurse",
                            "abbreviation": "RN",
                            "specialty": {
                                "id": 9,
                                "name": "Registered Nurse",
                                "color": "#F5657C",
                                "abbreviation": "RN"
                            }
                        }
                    }
                ]
            }
        ]
    }
    """.data(using: .utf8)!
    }
}
