import XCTest
@testable import CodingChallenge

final class ShiftsViewModelTests: XCTestCase {
    func test_PublishingOnError() throws {
        var service = ShiftsServiceMock()
        service.alwaysFails = true
        let sut = makeSUT(service: service)

        let exp = XCTestExpectation(description: "Publish onError")
        let cancellable = sut.onError
            .sink { _ in
                exp.fulfill()
            }

        wait(for: [exp], timeout: 1)
        XCTAssertNotNil(cancellable)
    }
    
    func test_loadSections() {
        let service = ShiftsServiceMock()
        let sut = makeSUT(service: service)

        let exp = XCTestExpectation(description: "Sections available")
        let cancellable = sut.$sections
            .sink { dayShifts in
                if !dayShifts.isEmpty {
                    exp.fulfill()
                }
            }
        wait(for: [exp], timeout: 1)
        XCTAssertNotNil(cancellable)
    }
    
    func test_selectShiftOpensSheet() {
        let service = ShiftsServiceMock()
        let sut = makeSUT(service: service)
        
        let mockShift = Shift(id: 0, shiftType: .day, distance: 20, startTime: Date(), endTime: Date(), details: nil)
        sut.didSelectShift(mockShift)

        let exp = XCTestExpectation(description: "Select shift opens sheet")
        let cancellable = sut.$activeLink
            .sink { contentLink in
                if contentLink == .sheetLink(shift: mockShift) {
                    exp.fulfill()
                }
            }
        wait(for: [exp], timeout: 1)
        XCTAssertNotNil(cancellable)
    }
    
    
    
    // MARK: - Helpers
    
    func makeSUT(service: ShiftsServiceProtocol) -> ShiftsViewModel {
        return ShiftsViewModel(shiftService: service)
    }
}
