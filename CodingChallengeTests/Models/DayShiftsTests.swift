import XCTest
@testable import CodingChallenge

final class DayShiftsTests: XCTestCase {
    func testDecoding() throws {
        let jsonDecoder = JSONDecoder()
        let response = try jsonDecoder.decode(ResponseData.self, from: MockData.response)
        let dayShiftDTO = response.data[0]
        let dayShift = dayShiftDTO.toDomain()
        XCTAssertNotNil(dayShiftDTO)
        XCTAssertEqual(dayShiftDTO.date, "2023-01-08")
        XCTAssertEqual(dayShift.date, "2023-01-08")
        XCTAssertTrue(!dayShiftDTO.shifts.isEmpty)
    }
}
