import XCTest
@testable import CodingChallenge

final class ShiftDetailsTests: XCTestCase {
    func testDecoding() throws {
        let jsonDecoder = JSONDecoder()
        let response = try jsonDecoder.decode(ResponseData.self, from: MockData.response)
        let shiftDTO = response.data[0].shifts[0]
        let shift = shiftDTO.toDomain()

        XCTAssertNotNil(shift.details)
        XCTAssertEqual(shift.details?.facility?.name, "Hospital")
        XCTAssertEqual(shift.details?.facility?.color, "#FF9500")
        XCTAssertEqual(shift.details?.covid, false)
        XCTAssertEqual(shift.details?.premium, false)
    }
}
