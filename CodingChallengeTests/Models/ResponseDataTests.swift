import XCTest
@testable import CodingChallenge

final class ResponseDataTests: XCTestCase {
    func testDecoding() throws {
        let jsonDecoder = JSONDecoder()
        let response = try jsonDecoder.decode(ResponseData.self, from: MockData.response)
        XCTAssertNotNil(response)
        XCTAssertNotNil(response.data)
    }
}
