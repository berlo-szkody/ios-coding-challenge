import XCTest
@testable import CodingChallenge

final class ShiftsTests: XCTestCase {
    func testDecoding() throws {
        let jsonDecoder = JSONDecoder()
        let response = try jsonDecoder.decode(ResponseData.self, from: MockData.response)
        let shiftDTO = response.data[0].shifts[0]
        let shift = shiftDTO.toDomain()
        // DTO
        XCTAssertNotNil(shiftDTO)
        XCTAssertEqual(shiftDTO.startTime, "2023-01-08T12:45:00+00:00")
        XCTAssertEqual(shiftDTO.endTime, "2023-01-09T01:15:00+00:00")
        XCTAssertEqual(shiftDTO.distance, 20)
        XCTAssertEqual(shiftDTO.shiftType, "Day Shift")
        XCTAssertEqual(shiftDTO.id, 4755666)
        // Domain
        XCTAssertNotNil(shift)
        XCTAssertEqual(shift.startTimeLabel, "13:45")
        XCTAssertEqual(shift.endTimeLabel, "02:15")
        XCTAssertEqual(shift.distance, 20)
        XCTAssertEqual(shift.shiftType, .day)
        XCTAssertEqual(shift.id, 4755666)
    }
}
